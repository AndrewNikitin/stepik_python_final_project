import time
import socket

class Client:

	def __init__(self, addr, port, timeout):
		self.addr = addr
		self.port = port
		self.timeout = timeout

	def put(self, key, value, timestamp=None):
		if timestamp is None:
			timestamp = str(int(time.time()))
		with socket.create_connection((self.addr, self.port), self.timeout) as sock:
			sock.sendall(f"put {key} {value} {timestamp}\n".encode('utf8'))
			response = sock.recv(1024).decode('utf8')
			if response != "ok\n\n":
				raise ClientError

	def get(self, key):
		with socket.create_connection((self.addr, self.port), self.timeout) as sock:
			sock.sendall(f"get {key}\n".encode('utf8'))
			status, *metrics = sock.recv(1024).decode('utf8').split('\n')
			if status != 'ok':
				raise ClientError
			result = {}
			for line in metrics:
				if not line:
					continue
				key, value, timestamp = line.split(' ')
				if key not in result:
					result[key] = []
				result[key].append((int(timestamp), float(value)))
			return result

class ClientError(Exception):
	pass